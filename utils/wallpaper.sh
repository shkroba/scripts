#! /bin/sh

get_current_wallpaper()
{
  wallpaper_setting_command="$(cat ~/.fehbg | tail -1)"
  quoted_path="$(echo "$wallpaper_setting_command" | rev | sed 's/^ *//' | cut -d ' ' -f 1 | rev)"
  
  echo "$quoted_path" | cut -c 2- | rev | cut -c 2- | rev
}

get_available_wallpapers()
{
  wallpapers_directory="$(realpath ~/.config/wallpaper/)"

  [ -d "$wallpapers_directory" ] || exit 1

  find "$wallpapers_directory" -type f -regex '.*\.\(png\|jpg\|jpeg\)'
}

get_signle_random()
{
  shuf -n 1
}

get_different_random_wallpaper()
{
  current_wallpaper="$1"
  wallpapers="$2"

  random_wallpaper="$(echo "$wallpapers" | get_signle_random)"

  while [ "$random_wallpaper" = "$current_wallpaper" ]
  do
    random_wallpaper="$(echo "$wallpapers" | get_signle_random)"
  done

  echo "$random_wallpaper"
}

set_wallpaper()
{
  feh --bg-scale "$1"
}
